import React from 'react';
import '../../css/TestCollapse.css';
import $ from 'jquery/dist/jquery.js';
import TrackDatainformation from './TrackDatainformation';
import TrackDatainformation2 from './TrackDatainformation2';

export default class Example extends React.Component {
    componentDidMount() {
        $("#sidebarCollapse").on("click", function () {
            $("#sidebar").toggleClass("d-none");
            $(this).toggleClass("active");
        });
    }
    render() {
        return (
            <div class="wrapperData container">
                <div className="px-2">
                    <TrackDatainformation />
                </div>
                <nav id="sidebar" className="d-none">
                    <div id="content" className="px-2">
                        <TrackDatainformation2 />
                    </div>
                </nav>
                <a id="sidebarCollapse" href="#" class="text-success" >
                <h4>>></h4></a>
            </div>
        );
    }
}
import React from 'react';
import $ from 'jquery/dist/jquery';
import '../../css/TestCollapse.css';
import Testcollapse from './Testcollapse';

export default class TrackDataModal extends React.Component {

  componentDidMount() {
    $("#addTrackData").draggable({
      handle: ".modal-header"
    });

    $('.modal').modal({
      backdrop: false,
      show: false
    });

    $("#sidebarCollapse").on("click", function () {
      $("#sidebar").toggleClass("active");
      $(this).toggleClass("active");
    });
  }

  render() {
    return (
      <div id="addTrackData" className="modal fade modeless">
        <div className="modal-dialog modal-lg" >
          <div className="modal-content">
            <div className="modal-header bg-success text-white">
              <h5 className="modal-title">Track Data</h5>
              <button className="close" data-dismiss="modal" >
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <Testcollapse />
            </div>
            <div className="modal-footer">
              <button className="btn btn-danger" data-dismiss="modal" >Save Changes</button>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
import React,{Component} from 'react';
import User from './User';

export default class Users extends React.Component{
    state={
        users:[
            {name:"john",age:20},
            {name:"Jill",age:30},
            {name:"Peter",age:40}
        ],title:"Users List"
    }
    render(){
        return(
            <div class="container">
                <h1>{this.state.title}</h1>
                <User age={this.state.users[0].age}>{this.state.users[0].name}</User>
                <User age={this.state.users[1].age}>{this.state.users[1].name}</User>
                <User age={this.state.users[2].age}>{this.state.users[2].name}</User>
                </div>
        );
    }
}
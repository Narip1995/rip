import React from 'react';
import Work_Enemy from './Track/Work_Enemy';
import Work_Total from './Track/Work_Total';
import {TabContent,TabPane} from 'reactstrap';

export default class Work extends React.Component {
    state = {
        activeTab:'1'
         };
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div class="container">

                <div class="btn-group">
                    <a class="btn btn-secondary" data-toggle="collapse" href="#collapseExample" role="button"
                        aria-expanded="false" aria-controls="collapseExample">collapse</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="ToggleMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {/* <span class="sr-only">Toggle dropright</span> */}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="ToggleMenu">
                            <a class="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('1'); }}>Total</a>
                            <a class="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('2'); }}>Enemy</a>
                            <a class="dropdown-item" href="#">Friend</a>
                            <a class="dropdown-item" href="#">Unknown</a>
                            <a class="dropdown-item" href="#">Arthur</a>
                            <a class="dropdown-item" href="#">G180</a>
                        </div>
                    </div>
                </div>

                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                    <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Work_Total/>
                    </TabPane>
                </TabContent>

                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="2">
                        <Work_Enemy/>
                    </TabPane>
                </TabContent>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        HelloWork
                            </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>



            </div>
        );
    }
}
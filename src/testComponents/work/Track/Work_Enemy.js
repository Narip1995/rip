import React from 'react';


export default class Work_Enemy extends React.Component {
    render() {
        return (
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light blue-grey lighten-5 mb-4">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item cative">
                                <a class="nav-link" href="#">Track Enemy</a>
                            </li>
                            <li class="nav-item cative">
                                <a class="nav-link" href="#">Category</a>
                            </li>
                            <li class="nav-item cative">
                                <a class="nav-link" href="#">Position</a>
                            </li>
                            <li class="nav-item cative">
                                <a class="nav-link" href="#">Altitude</a>
                            </li>
                            <li class="nav-item cative">
                                <a class="nav-link" href="#">Speed</a>
                            </li>
                        </ul>

                        <form class="form-inline">
                            <input class="form-control" type="text" placeholder="Search" aria-label="Search" />
                        </form>

                </nav>
            </div>
        );
    }
}
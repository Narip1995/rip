import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import TrackList_Total from './TrackList_Total';
import TrackList_Enemy from './TrackList_Enemy';
import '../../css/AnimatedRadioTab.css';
import Total from './TrackList_Total';
import { checkServerIdentity } from 'tls';
import AvCheckbox from 'availity-reactstrap-validation/lib/AvCheckbox';
import AvCheckboxGroup from 'availity-reactstrap-validation/lib/AvCheckboxGroup';

export default class TrackList extends React.Component {


    state = {
        activeTab:'1'
         };


    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div>
                
                <div class="wrapper" >
                    <div class="toggle_radio">
                        <input type="radio" class="toggle_option" id="Total_toggle" name="toggle_option" />
                        <input type="radio" class="toggle_option" id="Enemy_toggle" name="toggle_option" />
                        <input type="radio" class="toggle_option" id="Friend_toggle" name="toggle_option" />
                        <input type="radio" class="toggle_option" id="Unknown_toggle" name="toggle_option" />
                        <input type="radio" class="toggle_option" id="Arthur_toggle" name="toggle_option" />
                        <input type="radio" class="toggle_option" id="G180_toggle" name="toggle_option" />

                        <label for="Total_toggle"  onClick={() => { this.toggle('1'); }}><p>Total</p></label>
                        <label for="Enemy_toggle"  onClick={() => { this.toggle('2'); }}><p>Enemy</p></label>
                        <label for="Friend_toggle"   onClick={() => { this.toggle('3'); }}><p>Friend</p></label>
                        <label for="Unknown_toggle"   onClick={() => { this.toggle('4'); }}><p>Unknown</p></label>
                        <label for="Arthur_toggle"   onClick={() => { this.toggle('5'); }}><p>Arthur</p></label>
                        <label for="G180_toggle"   onClick={() => { this.toggle('6'); }}><p>G180</p></label>
                        <div class="toggle_option_slider">
                        </div>


                    </div>
                </div>
                

                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <TrackList_Total />
                    </TabPane>
                </TabContent>

                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="2">
                        <TrackList_Enemy />
                    </TabPane>
                </TabContent>

                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="3">
                    </TabPane>
                </TabContent>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="4">
                    </TabPane>
                </TabContent>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="5">
                    </TabPane>
                </TabContent>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="6">
                    </TabPane>
                </TabContent>
            </div>
        );
    }
}